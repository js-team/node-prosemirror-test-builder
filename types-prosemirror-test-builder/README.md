# Installation
> `npm install --save @types/prosemirror-test-builder`

# Summary
This package contains type definitions for prosemirror-test-builder (https://github.com/prosemirror/prosemirror-test-builder#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/prosemirror-test-builder.

### Additional Details
 * Last updated: Mon, 18 Jan 2021 15:07:08 GMT
 * Dependencies: [@types/prosemirror-model](https://npmjs.com/package/@types/prosemirror-model)
 * Global values: none

# Credits
These definitions were written by [Ifiok Jr.](https://github.com/ifiokjr).
